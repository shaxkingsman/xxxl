import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Header from "./component/react_dom/Header";
import Navigation from "./component/react_dom/navigation";
import Main from "./component/react_dom/main";
import Carusel from "./component/react_dom/carusel";



class NewProject extends Component {
    render() {
        return (
            <Router>
                <Header/>
                <Navigation/>
                <Main/>
                <Carusel/>
            </Router>
        );
    }
}

export default NewProject;