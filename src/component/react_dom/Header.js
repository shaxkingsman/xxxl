import React, {Component} from 'react';
import "bootstrap/dist/css/bootstrap.css";
import "../css_dom/Header.css";

import { Input, Space, Select, Menu, Dropdown, Button } from 'antd';


const { Search } = Input;
const { Option } = Select;


const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item
            </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                3rd menu item
            </a>
        </Menu.Item>
    </Menu>
);


class Header extends Component {



    render() {

        const onSearch = value => console.log(value);

        function handleChange(value) {
            console.log(`selected ${value}`);
        }

        return (
            <div>
                <div className="container">
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <ul className="navbar-nav">
                            <li className="navbar-brand"><img src="https://asaxiy.uz/custom-assets/images/logos/asaxiy-logo.svg" alt=""/></li>
                            <li className="nav-item">
                                <Space direction="vertical">
                                    <Search
                                        placeholder="input search text"
                                        allowClear
                                        enterButton="Search"
                                        size="large"
                                        onSearch={onSearch}
                                    />
                                </Space>
                            </li>
                            <li className="nav-item">
                                <Select defaultValue="lucy" style={{ width: 120 }} onChange={handleChange}>
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="bar">bar</Option>
                                </Select>
                            </li>
                            <li className="nav-item">
                                <Dropdown overlay={menu} placement="bottomCenter" arrow>
                                    <Button>bottomCenter</Button>
                                </Dropdown>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        );
    }
}

Header.propTypes = {};

export default Header;

